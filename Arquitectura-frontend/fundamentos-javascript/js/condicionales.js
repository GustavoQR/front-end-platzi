var gustavo = {
    nombre: 'Gustavo',
    apellido: 'Quispe',
    edad: 10,
    ingeniero: false,
    cocinero: false,
    cantante: false,
    dj: false,
    guitarrista: true,
    drones: false
}

function imprimirProfesiones(persona){
    console.log(`${persona.nombre} es: `);
    if (persona.ingeniero){
        console.log('Ingeniero');
    }else{
        console.log('No es Ingeniero');
    }
    if (persona.cocinero){
        console.log('Cocinero');
    }
    if (persona.cantante){
        console.log('Cantante');
    }
    if (persona.dj === true){
        console.log('Dj');
    }
    if (persona.guitarrista){
        console.log('Guitarrista');
    }
    if (persona.drones){
        console.log('Drones');
    }
}

function imprimirSiEsMayorDeEdad(persona){
    let {edad} = persona;
    let {nombre} = persona;
    if(edad>=18){
        console.log(`${nombre} es mayor de edad`);
    }else{
        console.log(`${nombre} es menor de edad`);
    }
}
imprimirSiEsMayorDeEdad(gustavo);
imprimirProfesiones(gustavo);