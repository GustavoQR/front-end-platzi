var gustavo = {
    nombre: 'Gustavo',
    apellido: 'Quispe',
    edad: 25,
    peso: 75
}

console.log(`Al inicio del año ${gustavo.nombre} ${gustavo.peso}kg`);
const DIAS_DEL_AÑO = 365;
const ICREMENTO_PESO = 0.3;

const aumentarDePeso = persona => persona.peso+=ICREMENTO_PESO
const adelgazar = persona => persona.peso-=ICREMENTO_PESO

const comeMucho = () => Math.random() < 0.3;
const realizarDeporte = () => Math.random() < 0.4;

const META = gustavo.peso -3;
var dias = 0;

while(gustavo.peso > META){

    if(comeMucho()){
       // aumentar de peso
       aumentarDePeso(gustavo); 
    }
    if(realizarDeporte()){
       // adelgazar 
       adelgazar(gustavo);
    }
    dias++;
}

console.log(`Pasaron ${dias} dias hasta que ${gustavo.nombre} adelgazo`);
