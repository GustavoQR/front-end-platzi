var gustavo = {
    nombre: 'Gustavo',
    apellido: 'Quispe',
    edad: 25,
    ingeniero: false,
    cocinero: false,
    cantante: false,
    dj: false,
    guitarrista: true,
    drones: false
}

var juan = {
    nombre: 'Juan',
    apellido: 'Gomez',
    edad: 13
}

function imprimirProfesiones(persona){
    console.log(`${persona.nombre} es: `);
    if (persona.ingeniero){
        console.log('Ingeniero');
    }else{
        console.log('No es Ingeniero');
    }
    if (persona.cocinero){
        console.log('Cocinero');
    }
    if (persona.cantante){
        console.log('Cantante');
    }
    if (persona.dj === true){
        console.log('Dj');
    }
    if (persona.guitarrista){
        console.log('Guitarrista');
    }
    if (persona.drones){
        console.log('Drones');
    }
}

const MAYORIA_EDAD = 18;

function esMayorDeEdad(edad){
  return edad>=MAYORIA_EDAD
}

function imprimirSiEsMayorDeEdad(persona){
    let {edad} = persona;
    let {nombre} = persona;
    if(esMayorDeEdad(edad)){
        console.log(`${nombre} es mayor de edad`);
    }else{
        console.log(`${nombre} es menor de edad`);
    }
}

imprimirSiEsMayorDeEdad(gustavo);
imprimirProfesiones(gustavo);