var gustavo = {
    nombre: 'Gustavo',
    apellido: 'Quispe',
    edad: 25,
    peso: 75
}

console.log(`Al inicio del año ${gustavo.nombre} ${gustavo.peso}kg`);
const DIAS_DEL_AÑO = 365;
const ICREMENTO_PESO = 0.2;

const aumentarDePeso = persona => persona.peso+=ICREMENTO_PESO
const adelgazar = persona => persona.peso-=ICREMENTO_PESO
for(var i=1;i<=DIAS_DEL_AÑO;i++){
    var random = Math.random();
    if(random < 0.25){
        aumentarDePeso(gustavo);
    }else if(random < 0.5){
        adelgazar(gustavo);
    }
}

console.log(`Al final del año ${gustavo.nombre} ${gustavo.peso.toFixed(1)}kg`);
