var gustavo = {
    nombre: 'Gustavo',
    apellido: 'Quispe',
    edad: 25
}

var dario = {
    nombre: 'Dario',
    apellido: 'Sunisky',
    edad: 27
}
// desglozar objetos
function imprimirNombreEnMayusculas(persona){
    //var nombre = persona.nombre
    var { nombre } = persona;
    console.log(nombre.toUpperCase());
}

function imprimirNombreYEdad(persona){
    var { nombre } = persona;
    var { edad } = persona;
    console.log(`Hola, me llamo ${nombre} y tengo ${edad} años`);
}

// imprimirNombreEnMayusculas(gustavo);
// imprimirNombreEnMayusculas(dario);
// imprimirNombreYEdad(gustavo);
// imprimirNombreYEdad(dario);


//podemos mantener al objeto original y devolver un nuevo objeto
 function cumpleanos(persona){
     return {
         //deslozar al objeto
         ...persona,
         edad : persona.edad+1
     }
 }




