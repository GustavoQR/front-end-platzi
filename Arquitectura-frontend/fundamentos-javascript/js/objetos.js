var gustavo = {
    nombre: 'Gustavo',
    apellido: 'Quispe',
    edad: 25
}

var dario = {
    nombre: 'Dario',
    apellido: 'Sunisky',
    edad: 27
}

function imprimirNombreEnMayusculas({ nombre }){
    console.log(nombre.toUpperCase());
}
imprimirNombreEnMayusculas(gustavo);
imprimirNombreEnMayusculas(dario);
imprimirNombreEnMayusculas({nombre: 'Pepito'});


