var nombre = 'Gustavo',apellido = 'Quispe';
//funcion para poner en mayusculas
var nombreEnMayusculas = nombre.toUpperCase();
//funcion para poner en minusculas
var apellidoEnMinusculas = apellido.toLowerCase();
//funcion para capturar un caracter dependiendo el indice que se mande
var primeraaLetraDelNombre = nombre.charAt(0);
//funcion para saber cuantas letras tiene un string
var cantidadDeLetrasDelNombre = nombre.length;
//Concatenar cadenas
var nombreCompleto = nombre +' '+ apellido;
//Interpolacion de elementos
var nombreCompleto2 = `${nombre} ${apellido.toUpperCase()}`;
//Obtener una cantidad de string 
var str = nombre.substr(1,2);